package com.adamglobal.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ViewRedirector {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		return "login";
	}
	
	  @RequestMapping(value = { "/products"}, method = RequestMethod.GET)
	    public String productsPage(ModelMap model) {
	        return "products";
	    }
	 
	  

}

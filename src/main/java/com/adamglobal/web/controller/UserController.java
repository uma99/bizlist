package com.adamglobal.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adamglobal.services.UserService;
import com.adamglobal.web.jsonview.Views;
import com.adamglobal.web.model.UserResponseBody;
import com.adamglobal.web.model.SearchCriteria;
import com.adamglobal.web.model.User;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
public class UserController {

	List<User> users;
	
	Logger logger = Logger.getLogger(UserController.class.getName());
	
	@Autowired
	 UserService service;
	 
	     
	
	
	/**
	 * 
	 * @param User
	 * @return SUCCESS / FAILURE / USER_ALREADY_EXSISTS
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/user/api/register")
	public UserResponseBody registerNewUser(@RequestBody User newUser) {
		logger.log(Level.INFO, "Registered User: "+newUser);
		UserResponseBody result = new UserResponseBody();
		result.setCode("200");
		service.saveUser(newUser);
		return result;

	}

	/**
	 * 
	 * @param User
	 * @return SUCCESS / FAILURE
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/user/api/login")
	public UserResponseBody loginUser(@RequestBody User user) {

		logger.log(Level.INFO, "Login User: "+user);
		
		UserResponseBody result = new UserResponseBody();
		User userView = service.searchUsersAuth(user);
		users = new ArrayList<User>();
		users.add(userView);
		result.setResult(users);
		result.setCode("200");
		if(result.getResult().size()>0){
			result.setMsg("SUCCESS");	
		}else{
			result.setMsg("FAIL");
		}
		
		return result;
	}

	
	/**
	 * 
	 * @param User
	 * @return EMAIL_SENT / EMAIL_NOT_FOUND
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/user/api/fyp")
	public UserResponseBody fypUser(@RequestBody User user) {
		logger.log(Level.INFO, "FYP User: "+user);
		UserResponseBody result = new UserResponseBody();
		result.setCode("200");
//		String userCreatedMessage = userService.sendEmail(user);
		String userCreatedMessage = "SUCCESS";
		result.setMsg(userCreatedMessage);
		return result;
	}
	
	/**
	 * 
	 * @param User
	 * @return EMAIL_SENT / EMAIL_NOT_FOUND
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/user/api/viewProfile")
	public UserResponseBody viewProfile(@RequestBody User user) {
		logger.log(Level.INFO, "ViewProfile User: "+user);
		
		UserResponseBody result = new UserResponseBody();
		result.setCode("200");
//		User userView = userService.viewProfile(user);
		User userView = service.findUserBySsn(user.getUsername());
		users = new ArrayList<User>();
		users.add(userView);
		result.setResult(users);

		return result;
	}
	
	
	/**
	 * 
	 * @param User
	 * @return SUCCESS / FAILED
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/user/api/editProfile")
	public UserResponseBody editProfile(@RequestBody User user) {
		logger.log(Level.INFO, "editProfile User: "+user);
		
		UserResponseBody result = new UserResponseBody();
		result.setCode("200");
		service.updateUser(user);
//		String userCreatedMessage = userService.editProfile(user);
		String userCreatedMessage = "SUCCESS";
		result.setMsg(userCreatedMessage);
		return result;
	}
	
	
	// @ResponseBody, not necessary, since class is annotated with @RestController
	// @RequestBody - Convert the json data into object (SearchCriteria) mapped by field name.
	// @JsonView(Views.Public.class) - Optional, limited the json data display to client.
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/user/api/search/username")
	public UserResponseBody searchUsersByUsername(@RequestBody User user) {
		logger.log(Level.INFO, "search User: "+user);
		
		UserResponseBody result = new UserResponseBody();
//			List<User> users =  userService.searchUsers(user);
		List<User> findAllUsers = service.searchUsersByUsername(user);
			result.setResult(findAllUsers);

		//AjaxResponseBody will be converted into json format and send back to client.
		return result;

	}

	// @ResponseBody, not necessary, since class is annotated with @RestController
	// @RequestBody - Convert the json data into object (SearchCriteria) mapped by field name.
	// @JsonView(Views.Public.class) - Optional, limited the json data display to client.
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/user/api/search")
	public UserResponseBody searchUsers(@RequestBody User user) {
		logger.log(Level.INFO, "search User: "+user);
		
		UserResponseBody result = new UserResponseBody();
		List<User> findAllUsers = service.searchUsers(user);
		result.setResult(findAllUsers);
		
		//AjaxResponseBody will be converted into json format and send back to client.
		return result;
		
	}
	
	

	// @ResponseBody, not necessary, since class is annotated with @RestController
	// @RequestBody - Convert the json data into object (SearchCriteria) mapped by field name.
	// @JsonView(Views.Public.class) - Optional, limited the json data display to client.
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/user/search/api/getSearchResult")
	public UserResponseBody getSearchResultViaAjax(@RequestBody SearchCriteria search) {

		UserResponseBody result = new UserResponseBody();

		if (isValidSearchCriteria(search)) {
			List<User> users = findByUserNameOrEmail(search.getUsername(), search.getEmail());

			if (users.size() > 0) {
				result.setCode("200");
				result.setMsg("");
				result.setResult(users);
			} else {
				result.setCode("204");
				result.setMsg("No user!");
			}

		} else {
			result.setCode("400");
			result.setMsg("Search criteria is empty!");
		}

		//AjaxResponseBody will be converted into json format and send back to client.
		return result;

	}

	private boolean isValidSearchCriteria(SearchCriteria search) {

		boolean valid = true;

		if (search == null) {
			valid = false;
		}

		if ((StringUtils.isEmpty(search.getUsername())) && (StringUtils.isEmpty(search.getEmail()))) {
			valid = false;
		}

		return valid;
	}

	// Init some users for testing
	@PostConstruct
	private void iniDataForTesting() {
		users = new ArrayList<User>();

	}

	// Simulate the search function
	private List<User> findByUserNameOrEmail(String username, String email) {

		List<User> result = new ArrayList<User>();

		for (User user : users) {

			if ((!StringUtils.isEmpty(username)) && (!StringUtils.isEmpty(email))) {

				if (username.equals(user.getUsername()) && email.equals(user.getEmail())) {
					result.add(user);
					continue;
				} else {
					continue;
				}

			}
			if (!StringUtils.isEmpty(username)) {
				if (username.equals(user.getUsername())) {
					result.add(user);
					continue;
				}
			}

			if (!StringUtils.isEmpty(email)) {
				if (email.equals(user.getEmail())) {
					result.add(user);
					continue;
				}
			}

		}

		return result;

	}
}

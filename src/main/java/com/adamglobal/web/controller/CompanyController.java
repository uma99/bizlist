package com.adamglobal.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adamglobal.services.CompanyService;
import com.adamglobal.web.jsonview.Views;
import com.adamglobal.web.model.Company;
import com.adamglobal.web.model.CompanyResponseBody;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
public class CompanyController {

	List<Company> companys;

	Logger logger = Logger.getLogger(CompanyController.class.getName());

	@Autowired
	CompanyService service;
	
	/**
	 * 
	 * @param Company
	 * @return SUCCESS / FAILURE / USER_ALREADY_EXSISTS
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/company/api/register")
	public CompanyResponseBody registerNewCompany(@RequestBody Company newCompany) {
		logger.log(Level.INFO, "Registered Company: "+newCompany);
		CompanyResponseBody result = new CompanyResponseBody();
		result.setCode("200");
		service.saveCompany(newCompany);
		return result;

	}

	/**
	 * 
	 * @param Company
	 * @return SUCCESS / FAILURE
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/company/api/login")
	public CompanyResponseBody loginCompany(@RequestBody Company company) {

		logger.log(Level.INFO, "Login Company: "+company);
		
		CompanyResponseBody result = new CompanyResponseBody();
		Company companyView = service.searchCompanysAuth(company);
		companys = new ArrayList<Company>();
		companys.add(companyView);
		result.setResult(companys);
		result.setCode("200");
		if(result.getResult().size()>0){
			result.setMsg("SUCCESS");	
		}else{
			result.setMsg("FAIL");
		}
		
		return result;
	}

	
	/**
	 * 
	 * @param Company
	 * @return EMAIL_SENT / EMAIL_NOT_FOUND
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/company/api/fyp")
	public CompanyResponseBody fypCompany(@RequestBody Company company) {
		logger.log(Level.INFO, "FYP Company: "+company);
		CompanyResponseBody result = new CompanyResponseBody();
		result.setCode("200");
//		String companyCreatedMessage = companyService.sendEmail(company);
		String companyCreatedMessage = "SUCCESS";
		result.setMsg(companyCreatedMessage);
		return result;
	}
	
	/**
	 * 
	 * @param Company
	 * @return EMAIL_SENT / EMAIL_NOT_FOUND
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/company/api/viewProfile")
	public CompanyResponseBody viewProfile(@RequestBody Company company) {
		logger.log(Level.INFO, "ViewProfile Company: "+company);
		
		CompanyResponseBody result = new CompanyResponseBody();
		result.setCode("200");
//		Company companyView = companyService.viewProfile(company);
		Company companyView = service.findCompanyBySsn(company.getName());
		companys = new ArrayList<Company>();
		companys.add(companyView);
		result.setResult(companys);

		return result;
	}
	
	
	/**
	 * 
	 * @param Company
	 * @return SUCCESS / FAILED
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/company/api/editProfile")
	public CompanyResponseBody editProfile(@RequestBody Company company) {
		logger.log(Level.INFO, "editProfile Company: "+company);
		
		CompanyResponseBody result = new CompanyResponseBody();
		result.setCode("200");
		service.updateCompany(company);
//		String companyCreatedMessage = companyService.editProfile(company);
		String companyCreatedMessage = "SUCCESS";
		result.setMsg(companyCreatedMessage);
		return result;
	}
	
	
	// @ResponseBody, not necessary, since class is annotated with @RestController
	// @RequestBody - Convert the json data into object (SearchCriteria) mapped by field name.
	// @JsonView(Views.Public.class) - Optional, limited the json data display to client.
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/company/api/search/companyname")
	public CompanyResponseBody searchCompanysByCompanyname(@RequestBody Company company) {
		logger.log(Level.INFO, "search Company: "+company);
		
		CompanyResponseBody result = new CompanyResponseBody();
//			List<Company> companys =  companyService.searchCompanys(company);
		List<Company> findAllCompanys = service.searchCompanysByCompanyname(company);
			result.setResult(findAllCompanys);

		//AjaxResponseBody will be converted into json format and send back to client.
		return result;

	}

	// @ResponseBody, not necessary, since class is annotated with @RestController
	// @RequestBody - Convert the json data into object (SearchCriteria) mapped by field name.
	// @JsonView(Views.Public.class) - Optional, limited the json data display to client.
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/company/api/search")
	public CompanyResponseBody searchCompanys(@RequestBody Company company) {
		logger.log(Level.INFO, "search Company: "+company);
		
		CompanyResponseBody result = new CompanyResponseBody();
		List<Company> findAllCompanys = service.searchCompanys(company);
		result.setResult(findAllCompanys);
		
		//AjaxResponseBody will be converted into json format and send back to client.
		return result;
		
	}
	


}

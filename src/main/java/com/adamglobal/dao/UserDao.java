package com.adamglobal.dao;

import java.util.List;

import com.adamglobal.web.model.User;
 
 
public interface UserDao {
 
    User findById(int id);
 
    void saveUser(User User);
     
    void deleteUserBySsn(String ssn);
     
    List<User> findAllUsers();
 
    User findUserBySsn(String ssn);
    
    void updateUser(User User);

	List<User> searchUsers(User user);

	List<User> searchUsersByUsername(String user);

	User searchUsersAuth(String username, String password);

 
}
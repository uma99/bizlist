package com.adamglobal.dao;

import java.util.List;
import com.adamglobal.web.model.Company;
 
 
public interface CompanyDao {
 
    Company findById(int id);
 
    void saveCompany(Company Company);
     
    void deleteCompanyBySsn(String ssn);
     
    List<Company> findAllCompanys();
 
    Company findCompanyBySsn(String ssn);
    
    void updateCompany(Company company);

	List<Company> searchCompanys(Company company);

	List<Company> searchCompanysByCompanyname(String company);

	Company searchCompanysAuth(String username, String password);
 
}
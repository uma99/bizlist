package com.adamglobal.services;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adamglobal.dao.CompanyDao;
import com.adamglobal.web.model.Company;

@Service("companyService")
@Transactional
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyDao dao;

	@Autowired
	MessageSource messageSource;

	public Company findById(int id) {
		return dao.findById(id);
	}

	public void saveCompany(Company Company) {
		dao.saveCompany(Company);
	}

	/*
	 * Since the method is running with Transaction, No need to call hibernate
	 * update explicitly. Just fetch the entity from db and update it with
	 * proper values within transaction. It will be updated in db once
	 * transaction ends.
	 */
	public void updateCompany(Company Company) {
		// Company entity = dao.findById(Company.getId());
		Company entity = dao.findCompanyBySsn(Company.getName());
		System.out.println(" to be udpated Company s:" + entity);
		if (entity != null) {
			entity.setName(Company.getName());
			entity.setPassword(Company.getPassword());
			entity.setEmail(Company.getEmail());
			entity.setPhone(Company.getPhone());
			entity.setAddress(Company.getAddress());
		}
		dao.updateCompany(entity);

	}

	public void deleteCompanyBySsn(String ssn) {
		dao.deleteCompanyBySsn(ssn);
	}

	public List<Company> findAllCompanys() {
		return dao.findAllCompanys();
	}

	public Company findCompanyBySsn(String ssn) {
		return dao.findCompanyBySsn(ssn);
	}

	public boolean isCompanySsnUnique(Integer id, String ssn) {
		Company Company = findCompanyBySsn(ssn);
		return (Company == null || ((id != null) && (Company.getId() == id)));
	}

	@Override
	public List<Company> searchCompanys(Company Company) {
		// TODO Auto-generated method stub
		return dao.searchCompanys(Company);
	}

	@Override
	public List<Company> searchCompanysByCompanyname(Company Company) {
		// TODO Auto-generated method stub
		return dao.searchCompanysByCompanyname(Company.getName());
	}

	@Override
	public Company searchCompanysAuth(Company company) {
		// TODO Auto-generated method stub
		return dao.searchCompanysAuth(company.getName(), company.getPassword());
	}

}
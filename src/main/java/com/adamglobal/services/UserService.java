package com.adamglobal.services;

import java.util.List;

import com.adamglobal.web.model.SearchCriteria;
import com.adamglobal.web.model.User;

public interface UserService {


	User findById(int id);

	void saveUser(User User);

	void updateUser(User User);

	void deleteUserBySsn(String ssn);

	List<User> findAllUsers();

	User findUserBySsn(String ssn);

	boolean isUserSsnUnique(Integer id, String ssn);

	List<User> searchUsers(User user);

	List<User> searchUsersByUsername(User user);

	User searchUsersAuth(User user);

}

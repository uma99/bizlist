package com.adamglobal.services;

import java.util.List;

import com.adamglobal.web.model.Company;

public interface CompanyService {


	Company findById(int id);

	void saveCompany(Company company);

	void updateCompany(Company company);

	void deleteCompanyBySsn(String ssn);

	List<Company> findAllCompanys();

	Company findCompanyBySsn(String ssn);

	boolean isCompanySsnUnique(Integer id, String ssn);

	List<Company> searchCompanys(Company company);

	List<Company> searchCompanysByCompanyname(Company company);

	Company searchCompanysAuth(Company company);

}
